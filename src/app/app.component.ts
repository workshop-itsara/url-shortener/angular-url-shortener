import { Component } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private http: HttpClient
  ) { }

  urlShortenerColumns: string[] = ['long_url', 'short_url', 'count', 'tool'];
  urlShortener: any;
  urlShortenerDetail: any = {
    longUrl: ''
  }

  ngOnInit() {
    this.getUrlShortenerAll();
  }

  getUrlShortenerAll() {
    this.http.get(`${environment.apiPath}/url-shortener/get-url-shortener`).subscribe((res) => this.urlShortener = res);
  }

  getLongUrl(shortUrl: string) {
    window.open(shortUrl);
    setTimeout(() => this.getUrlShortenerAll(), 2);
  }

  createUrlShortener() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'Do you want to save data!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33'
    }).then((result) => {
      if (result.isConfirmed) {
        this.http.post(`${environment.apiPath}/url-shortener/create-url-shortener`, {
          long_url: this.urlShortenerDetail['longUrl']
        }).subscribe({
          next: () => {
            this.urlShortenerDetail['longUrl'] = '';
            this.getUrlShortenerAll();
            Swal.fire('Created!', 'Your data has been created.', 'success');
          },
          error: (err) => Swal.fire('Warning!', err['error']['message'], 'warning')
        });
      }
    })
  }

  deleteUrlShortener(id: string) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'Do you want to delete data!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33'
    }).then((result) => {
      if (result.isConfirmed) {
        this.http.delete(`${environment.apiPath}/url-shortener/delete-url-shortener/${id}`).subscribe({
          next: () => {
            this.getUrlShortenerAll();
            Swal.fire('Deleted!', 'Your data has been deleted.', 'success');
          },
          error: (err) => Swal.fire('Warning!', err['error']['message'], 'warning')
        });
      }
    });
  }
}
