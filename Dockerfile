FROM node:16-alpine AS development
WORKDIR /usr/src/app
COPY package*.json .
RUN npm i
COPY . .

FROM node:16-alpine AS build
WORKDIR /usr/src/app
COPY package*.json .
COPY --from=development /usr/src/app/node_modules ./node_modules
COPY . .
RUN npm run build

FROM nginx:1.24.0-alpine-slim AS production
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /usr/src/app/dist /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]